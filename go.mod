module modernc.org/builder

go 1.20

require (
	github.com/golang/glog v1.1.1
	golang.org/x/mod v0.5.1
	modernc.org/gomod v1.2.2
	modernc.org/strutil v1.2.0
)

require golang.org/x/xerrors v0.0.0-20191011141410-1b5146add898 // indirect
