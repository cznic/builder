// Copyright 2020 The Builder Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package builder

import (
	"bytes"
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"math/rand"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"runtime"
	"sort"
	"strconv"
	"strings"
	"testing"
	"time"

	"github.com/golang/glog"
	"golang.org/x/mod/semver"
	gomod "modernc.org/gomod/engine"
	"modernc.org/strutil"
)

const (
	builderCfgFile = "builder.json"
	builderFile    = ".builder"
	gitMax         = 10 * time.Minute
	//TODO- gitIdle        = 2 * time.Minute
	goMax       = 5 * time.Minute
	idleMax     = 180 * time.Second
	idleMin     = 2 * time.Minute
	jobMax      = 24 * time.Hour
	logLimit    = 1 << 12
	maxRun      = 12 * time.Hour
	resultsFile = "results"
	tmax        = "24h"
	uploadAfter = time.Second
)

var (
	builder      string
	excludeDir   string
	goVersion    = runtime.Version()
	goVersions   = map[resultKey]string{}
	goarch       = runtime.GOARCH
	goos         = runtime.GOOS
	lastGitNetOp time.Time
	leader       = "nuc64" // this builder can auto {tag,update}
	logDir       string
	oDBG         = flag.Bool("dbg", false, "")
	oLogAuto     = flag.Bool("logauto", true, "")
	oRe          = flag.String("re", "", "")
	oRetry       = flag.Bool("retry", false, "")
	oStream      = flag.Bool("stream", true, "")
	oTest        = flag.Bool("test", false, "ignore builder.json:test")
	purgeKeys    []resultKey
	re           *regexp.Regexp
	results      = map[resultKey]string{}
	target       = fmt.Sprintf("%s/%s", goos, goarch)
	tasks        []task
	tempDir      string

	libs = map[string]struct{}{
		"darwin/amd64":   {},
		"darwin/arm64":   {},
		"freebsd/amd64":  {},
		"freebsd/arm64":  {},
		"linux/386":      {},
		"linux/amd64":    {},
		"linux/arm":      {},
		"linux/arm64":    {},
		"linux/loong64":  {},
		"linux/mips64le": {},
		"linux/ppc64le":  {},
		"linux/riscv64":  {},
		"linux/s390x":    {},
	}

	sqlite = map[string]struct{}{
		"darwin/amd64":  {},
		"darwin/arm64":  {},
		"freebsd/amd64": {},
		"freebsd/arm64": {},
		"linux/386":     {},
		"linux/amd64":   {},
		"linux/arm":     {},
		"linux/arm64":   {},
		"linux/loong64": {},
		//TODO "linux/mips64le": {},
		"linux/ppc64le": {},
		"linux/riscv64": {},
		"linux/s390x":   {},
		"windows/amd64": {},
		"windows/arm64": {},
	}

	libqbe = map[string]struct{}{
		"darwin/amd64":   {},
		"darwin/arm64":   {},
		"freebsd/amd64":  {},
		"freebsd/arm64":  {},
		"linux/386":      {},
		"linux/amd64":    {},
		"linux/arm":      {},
		"linux/arm64":    {},
		"linux/loong64":  {},
		"linux/mips64le": {},
		"linux/ppc64le":  {},
		"linux/riscv64":  {},
		"netbsd/amd64":   {},
		"openbsd/amd64":  {},
		"openbsd/arm64":  {},
	}

	defaultGoGet  = []string{"go", "get", "-t", "-v", "-u", "@"}
	defaultGoTest = []string{"go", "test", "-vet", "off", "-timeout", "36h", "-failfast"}

	needUpload bool
)

func gitNetLimiter() {
	defer func() {
		lastGitNetOp = time.Now()
	}()

	if lastGitNetOp.IsZero() {
		return
	}

	idle := idleMin + time.Duration(rand.Int63n(int64(idleMax-idleMin)))
	s := time.Since(lastGitNetOp)
	if s >= idle {
		return
	}

	w := idle - s
	infof("%s: git idle for %v", time.Now().Format(time.DateTime), w)
	time.Sleep(w)
}

func origin(skip int) string {
	pc, fn, fl, _ := runtime.Caller(skip)
	f := runtime.FuncForPC(pc)
	var fns string
	if f != nil {
		fns = f.Name()
		if x := strings.LastIndex(fns, "."); x > 0 {
			fns = fns[x+1:]
		}
	}
	return fmt.Sprintf("%s:%d:%s", filepath.Base(fn), fl, fns)
}

func todo(s string, args ...interface{}) string {
	switch {
	case s == "":
		s = fmt.Sprintf(strings.Repeat("%v ", len(args)), args...)
	default:
		s = fmt.Sprintf(s, args...)
	}
	r := fmt.Sprintf("%s\n\tTODO %s", origin(2), s) //TODOOK
	fmt.Fprintf(os.Stdout, "%s\n", r)
	os.Stdout.Sync()
	return r
}

func trc(s string, args ...interface{}) string {
	switch {
	case s == "":
		s = fmt.Sprintf(strings.Repeat("%v ", len(args)), args...)
	default:
		s = fmt.Sprintf(s, args...)
	}
	r := fmt.Sprintf("%s: TRC %s", origin(2), s)
	fmt.Fprintf(os.Stderr, "%s\n", r)
	info(r)
	os.Stderr.Sync()
	return r
}

func info(v any) {
	fmt.Fprintf(os.Stderr, "%v: %s INFO %s\n", origin(2), time.Now().Format(time.DateTime), fmt.Sprint(v))
	glog.Info(v)
}

func infof(s string, v ...any) {
	fmt.Fprintf(os.Stderr, "%v: %s INFO %s\n", origin(2), time.Now().Format(time.DateTime), fmt.Sprintf(s, v...))
	glog.Infof(s, v...)
}

func gerror(v any) {
	fmt.Fprintf(os.Stderr, "%v: %s FAIL %s\n", origin(2), time.Now().Format(time.DateTime), fmt.Sprint(v))
	glog.Error(v)
}

func errorf(s string, v ...any) {
	fmt.Fprintf(os.Stderr, "%v: %s FAIL %s\n", origin(2), time.Now().Format(time.DateTime), fmt.Sprintf(s, v...))
	glog.Errorf(s, v...)
}

// k={pi400 modernc.org/tcl} v=2023-09-22T16:52:46+02:00	eeb09a874245b41a6720d97279954f96f1c252ff	windows	arm64	FAIL	go1.21.1
// 	0:	2023-09-22T16:52:46+02:00
//	1:	eeb09a874245b41a6720d97279954f96f1c252ff
//	2:	windows
//	3:	arm64
//	4:	FAIL
//	5:	go1.21.1

func resultField(s string, n int) (r string) {
	if a := strings.Split(s, "\t"); len(a) > n {
		r = a[n]
	}
	return r
}

func resultTime(s string) string      { return resultField(s, 0) }
func resultCommit(s string) string    { return resultField(s, 1) }
func resultOS(s string) string        { return resultField(s, 2) }
func resultArch(s string) string      { return resultField(s, 3) }
func result(s string) string          { return resultField(s, 4) }
func resultGoVersion(s string) string { return resultField(s, 5) }

func isLib(os, arch string) bool {
	_, ok := libs[fmt.Sprintf("%s/%s", os, arch)]
	return ok
}

func isSqlite(os, arch string) bool {
	_, ok := sqlite[fmt.Sprintf("%s/%s", os, arch)]
	return ok
}

func isLibqbe(os, arch string) bool {
	_, ok := libqbe[fmt.Sprintf("%s/%s", os, arch)]
	return ok
}

type resultKey struct {
	builder  string
	testPath string
}

type task struct {
	branch     string
	builder    string
	canUpdate  []*gomod.Update
	hash       string
	importPath string
	test       []string
	testPath   string

	modUpdated bool
	needTest   bool
}

func (task *task) diskPath() string {
	return filepath.Join(excludeDir, filepath.FromSlash(task.importPath))
}

func (task *task) run() bool {
	infof("runTest(%q) entered", task.importPath)
	commit, err := task.commitHash()
	if err != nil {
		return false
	}

	var have string
	key := resultKey{builder: builder, testPath: task.testPath}
	if builder == "test" {
		key = resultKey{builder: "nuc64", testPath: task.testPath}
	}
	result, ok := results[key]
	if ok {
		a := strings.Split(result, "\t")
		if len(a) >= 2 {
			have = a[1]
		}
	}

	fmt.Printf("goVersions[%q]=%s commit=%s have=%s\n", key, goVersions[key], commit, have)
	task.needTest = true
	if !*oRetry && goVersions[key] == goVersion && commit == have {
		//infof("dbg commit %q have %q", commit, have)
		task.needTest = false
	}

	shell(goMax, ".", "go", "clean", "-testcache")
	infof("%v: checking %s@%s\n", timeNow(), task.importPath, commit)
	fmt.Printf("%v: checking %s@%s\n", timeNow(), task.importPath, commit)
	importPath := task.importPath
	testCmd := task.test
	if testCmd == nil {
		testCmd = append([]string(nil), defaultGoTest...)
	}
	for i, v := range testCmd {
		if v == "@" {
			testCmd[i] = importPath
		}
	}

	testPath := importPath
	if task.testPath != "" {
		testPath = task.testPath
	}
	update := false
	for _, testPath2 := range strings.Split(testPath, "|") {
		testPath2 = filepath.Join(excludeDir, testPath2)
		infof("dir %v, cmd %v", testPath2, testCmd)
		fmt.Printf("dir %v, cmd %v\n", testPath2, testCmd)
		cfg, err := loadBuilderConfig(filepath.Join(testPath2, builderCfgFile))
		if err != nil {
			errorf("loadBuilderConfig(%q) -> FAIL err=%s", builderCfgFile, err)
			return false
		}

		cfg.purgeResults(testPath)
		doTest := false
		if cfg != nil {
			var err error
			if doTest, err = targetMatches(target, cfg.Test); err != nil {
				errorf("regexp.Compile(%q) -> FAIL err=%s", cfg.Test, err)
				return false
			}

			switch out, err := task.autoTasks(cfg, testPath2); {
			case err != nil:
				errorf("FAIL err=%v autoTasksNew %s: %s FAIL err=%s", err, importPath, out, err)
				return false
			default:
				commit = task.hash
			}
		}

		if !doTest && !*oTest {
			continue
		}

		infof("task.needTest=%v", task.needTest)
		if !task.needTest {
			continue
		}

		if builder == "test" {
			infof("SKIP testing: builder=%v", builder)
			continue
		}

		if out, err := shell(jobMax, testPath2, testCmd[0], testCmd[1:]...); err != nil {
			errorf("FAIL testing importPath=%s testPath2=%v testCmd[0]=%v testCmd[1:]=%v FAIL err=%v", importPath, testPath2, testCmd[0], testCmd[1:], err)
			results[resultKey{builder: builder, testPath: testPath}] = newResult(commit, "FAIL")
			task.log(out, err)
			return false
		}

		update = true
	}

	if update {
		infof("PASS testing %s", importPath)
		results[resultKey{builder: builder, testPath: testPath}] = newResult(commit, "PASS")
	}
	return true
}

func (task *task) log(b []byte, err error) {
	if task.importPath == "" {
		panic(todo("internal error"))
	}

	b = append(b, fmt.Sprintf("\nFAIL err=%v", err)...)
	c := bytes.NewBuffer(nil)
	fmt.Fprintf(c, "%s commit %v, GOMAXPROCS %d, GOGC %q, GOMEMLIMIT %q, CC %q\n",
		time.Now().Format(time.RFC3339), task.hash, runtime.GOMAXPROCS(0), os.Getenv("GOGC"), os.Getenv("GOMEMLIMIT"), os.Getenv("CC"))
	switch {
	case len(b) > logLimit:
		c.Write(b[:logLimit/2])
		c.WriteString("\n\n...\n\n")
		c.Write(b[len(b)-logLimit/2:])
	default:
		c.Write(b)
	}
	path := filepath.Join(logDir, task.testPath)
	if err := os.MkdirAll(path, 0770); err != nil {
		gerror(err)
		return
	}

	path = filepath.Join(path, builder)
	if err := os.WriteFile(path, c.Bytes(), 0660); err != nil {
		gerror(err)
	}
}

func newResult(hash, result string) string {
	return strings.Join([]string{
		time.Now().Format(time.RFC3339), //	2	time
		hash,                            //	3	commit hash
		goos,                            //	4	os
		goarch,                          //	5	arch
		result,                          //	6	result, will contain PASS or FAIL
		goVersion},                      //	7	Go version
		"\t",
	)
}

func (task *task) tagHash(tag string) (h string, err error) {
	out, err := shell(gitMax, "", "git", "-C", task.diskPath(), "rev-list", "-n", "1", tag)
	if err != nil {
		errorf("FAIL err=%v\n%v", err, out)
		return "", err
	}

	h = strings.TrimSpace(string(out))
	task.hash = h
	return h, nil
}

func (task *task) commitHash() (h string, err error) {
	out, err := shell(gitMax, "", "git", "-C", task.diskPath(), "rev-parse", "HEAD")
	if err != nil {
		errorf("FAIL err=%v\n%v", err, out)
		return "", err
	}

	h = strings.TrimSpace(string(out))
	task.hash = h
	return h, nil
}

func TestMain(m *testing.M) {
	os.Setenv("MODERNC_BUILDER", "1")
	os.Exit(testMain(m))
}

func testMain(m *testing.M) int {
	fmt.Printf("test binary compiled for %s/%s, GOOS=%q, GOARCH=%q\n", goos, goarch, os.Getenv("GOOS"), os.Getenv("GOARCH"))
	fmt.Printf("Go version %s\n", runtime.Version())
	var err error
	if excludeDir, err = filepath.Abs(".exclude"); err != nil {
		panic("internal error")
	}

	if logDir, err = filepath.Abs("logs"); err != nil {
		panic("internal error")
	}

	verbose := false
	for _, v := range os.Args {
		if v == "-v" {
			verbose = true
			os.Args = append(os.Args, "-alsologtostderr")
			break
		}
	}
	flag.Parse()
	if verbose {
		*oStream = true
		*oLogAuto = true
	}
	if s := *oRe; s != "" {
		re = regexp.MustCompile(s)
	}
	lockFile := filepath.Join(excludeDir, ".single.lock")
	if fi, err := os.Stat(lockFile); err == nil {
		ok := false
		switch {
		case time.Since(fi.ModTime()) >= time.Hour*24:
			if runtime.GOOS == "windows" {
				break
			}

			out, _ := shell(time.Minute, "", "pgrep", "-l", "builder.test")
			a := strings.Split(string(out), "\n")
			mypid := os.Getpid()
			for _, v := range a {
				b := strings.Fields(v)
				if len(b) == 0 {
					continue
				}

				n, err := strconv.ParseUint(b[0], 10, 64)
				if err != nil {
					continue
				}

				if n != uint64(mypid) {
					shell(time.Minute, "", "kill", b[0])
				}
			}
			os.Remove(lockFile)
			ok = true
		}

		if !ok {
			b, err := os.ReadFile(lockFile)
			if err != nil {
				b = nil
			}
			fmt.Printf("other instance is already/still running, or there's a stale lock: %s %v %q\n", lockFile, fi.Size(), b)
			return 1
		}
	}

	s := fmt.Sprintf("%d\n", os.Getpid())
	if err := os.WriteFile(lockFile, []byte(s), 0600); err != nil {
		fmt.Fprintf(os.Stderr, "cannot create lock file: %v", err)
		return 1
	}

	defer func() {
		os.Remove(lockFile)
	}()

	if builder = os.Getenv("BUILDER"); builder == "" {
		nm := filepath.Join(excludeDir, builderFile)
		b, err := ioutil.ReadFile(nm)
		if err != nil {
			fmt.Fprintf(os.Stderr, "builder not initialized, exec '$ echo <name> > .exclude/.builder'")
			return 1
		}

		builder = strings.TrimSpace(string(b))
	}
	if builder != "test" {
		ok := false
		var out []byte
		for i := 0; i < 3; i++ {
			cleanRepo()
			if out, err = shell(gitMax, "", "git", "checkout", "-f"); err != nil {
				continue
			}

			if out, err = shell(gitMax, "", "git", "pull"); err != nil {
				continue
			}

			ok = true
			break
		}
		if !ok {
			fmt.Fprintf(os.Stderr, "%s\nFAIL err=%v\n", out, err)
			return 1
		}
	}

	rc := m.Run()
	infof("return code %v", rc)
	switch builder {
	case "test":
		fmt.Fprintf(os.Stderr, "test builder: not uploading results\n")
	default:
		glog.Flush()
		uploadResults()
	}
	return rc
}

func uploadResults() {
	if !needUpload {
		return
	}

	//  1. git pull ('master').
	//  2. load 'results', possibly meanwhile updated by other builders, into 'final'.
	//  3. update 'final' with own results from 'results'.
	//  4. git delete branch 'tmp'.
	//  5. git create new branch 'tmp' (from 'master').
	//  6. save 'final' into 'results'.
	//  7. git add 'logs/'.
	//  8. git commit ('tmp')
	//  9. git checkout 'master'.
	// 10. git merge 'tmp' into 'master'.
	// 11. git commit ('master').
	// 12. git push ('master')

	out, err := shell(gitMax, "", "git", "pull") // 1.
	if err != nil {
		errorf("FAIL err=%v\n%s", err, out)
		return
	}

	// 2.
	final, _, err := loadResults("results") // May have got updated by other builders
	needUpload = len(purgeKeys) != 0
	infof("needUpload=%v len(purgeKeys)=%v", needUpload, len(purgeKeys))
	infof("loaded %v lines into final", len(final))
	for k, v := range results { // 3.
		if k.builder == builder && final[k] != v {
			infof("updating {%v, %v} to %v", k.builder, k.testPath, v)
			final[k] = v // Update only our own results, not holding back other builders.
			needUpload = true
		}
	}

	if !needUpload {
		infof("not uploading anything")
		return
	}

	shell(gitMax, "", "git", "branch", "-D", "tmp")                                // 4.
	if out, err := shell(gitMax, "", "git", "checkout", "-b", "tmp"); err != nil { // 5.
		errorf("FAIL err=%v\n%s", err, out)
		return
	}

	infof("saved %v lines into final", len(final)) // 6.
	if err := saveResults("results", final); err != nil {
		errorf("FAIL err=%s", err)
		return
	}

	if out, err = shell(gitMax, "", "git", "add", "logs"); err != nil { // 7.
		errorf("FAIL err=%v\n%s", err, out)
		return
	}

	shell(gitMax, "", "git", "commit", "-am", builder)                          // 8.
	if out, err := shell(gitMax, "", "git", "checkout", "master"); err != nil { // 9.
		errorf("FAIL err=%v\n%s", err, out)
		return
	}

	if out, err := shell(gitMax, "", "git", "merge", "--squash", "tmp"); err != nil { // 10.
		errorf("FAIL err=%v\n%s", err, out)
		return
	}

	shell(gitMax, "", "git", "commit", "-am", builder) // 11.
	if out, err := shell(gitMax, "", "git", "checkout", "master"); err != nil {
		errorf("FAIL err=%v\n%s", err, out)
		return
	}

	infof("output of merge before push:\n%s", out)
	if out, err := shell(gitMax, "", "git", "push"); err != nil { // 12.
		errorf("FAIL err=%v\n%s", err, out)
	} else {
		needUpload = false
	}
}

func wipe(timeString string) (r bool) {
	// defer func() {
	// 	trc("wipe(%q) -> r=%v (%v: %v: %v)", timeString, r, origin(4), origin(3), origin(2))
	// }()
	const (
		wipeYear  = 2024
		wipeMonth = 9
		wipeDay   = 3
		wipeHour  = 10
	)

	t, err := time.Parse(time.RFC3339, timeString)
	if err != nil {
		// trc("err=%v", err)
		return false
	}

	y, m, d, h := t.Year(), t.Month(), t.Day(), t.Hour()
	// trc("y=%v m=%v d=%v", y, m, d)
	if y < wipeYear {
		return true
	}

	if y > wipeYear {
		return false
	}

	// y == wipeYear
	if m < wipeMonth {
		return true
	}

	if m > wipeMonth {
		return false
	}

	// m == wipeMonth
	if d < wipeDay {
		return true
	}

	if d > wipeDay {
		return false
	}

	//  d == wipeDay
	return h < wipeHour
}

func saveResults(fn string, m map[resultKey]string) error {
	for k, v := range m {
		// trc("k=%q v=%q", k, v)
		a := strings.Split(v, "\t")
		// trc("a=%q", a)
		if len(a) != 0 && wipe(a[0]) {
			delete(m, k)
		}
	}
	for _, k := range purgeKeys {
		infof("deleting %+v", k)
		delete(m, k)
	}
	var a []resultKey
	for k := range m {
		a = append(a, k)
	}
	sort.Slice(a, func(i, j int) bool {
		x := a[i]
		y := a[j]
		return x.testPath < y.testPath ||
			x.testPath == y.testPath && x.builder < y.builder
	})
	var b []string
	for _, k := range a {
		b = append(b, fmt.Sprintf("%v\t%v\t%v", k.testPath, k.builder, strings.TrimSpace(m[k])))
	}
	b2 := []byte(strings.Join(b, "\n") + "\n")
	err := ioutil.WriteFile(fn, b2, 0660)
	// infof("saved %s, %v\n%s", fn, err, b2)
	return err
}

func loadResults(fn string) (m, m2 map[resultKey]string, err error) {
	b, err := ioutil.ReadFile(fn)
	if err != nil {
		return nil, nil, err
	}

	// infof("loaded %s\n%s", fn, b)
	m = map[resultKey]string{}
	m2 = map[resultKey]string{}
	a := strings.Split(strings.TrimSpace(string(b)), "\n")
	for _, v := range a {
		if v = strings.TrimSpace(v); v == "" {
			continue
		}

		b := strings.SplitN(v, "\t", 3)
		key := resultKey{testPath: b[0], builder: b[1]}
		m[key] = b[2]
		if b = strings.Split(v, "\t"); len(b) >= 8 {
			m2[key] = b[7]
		}
	}
	return m, m2, nil
}

func Test(t *testing.T) {
	infof("Local time/zone: %v", time.Now().In(time.Local))
	fmt.Printf("Local time/zone: %v\n", time.Now().In(time.Local))
	out, err := shell(gitMax, "", "git", "log", "-1")
	if err != nil {
		errorf("FAIL err=%v\n%s", err, out)
		t.Errorf("FAIL err=%v\n%s", err, out)
		return
	}

	infof("builder repository commit: %s", strings.Split(string(out), "\n")[0])
	initTasks()
	w := 0
	for _, v := range tasks {
		if v.builder == builder {
			tasks[w] = v
			w++
		}
	}
	tasks = tasks[:w]
	if len(tasks) == 0 {
		return
	}

	if results, goVersions, err = loadResults("results"); err != nil {
		gerror(err)
		t.Fatal(err)
	}

	os.Setenv("GO111MODULE", "on")
	os.Setenv("INSTALL_ECC", "1")
	t0 := time.Now()
	uploaded := t0
	for i, task := range tasks {
		if time.Since(t0) > maxRun {
			break
		}

		if re != nil && !re.MatchString(task.testPath) {
			// infof("regexp does not match")
			continue
		}

		fmt.Printf("%v: task=%+v\n", timeNow(), task)
		if task.cloneOrUpdateRepository() {
			needUpload = true
			task.run()
		}
		if time.Since(uploaded) > uploadAfter {
			uploadResults()
			uploaded = time.Now()
		}
		if i < len(tasks)-1 {
			time.Sleep(idleMin + time.Duration(rand.Int63n(int64(idleMax-idleMin))))
		}
	}
}

func (task *task) cloneOrUpdateRepository() (r bool) {
	if builder == "test" {
		return true
	}

	dir := task.diskPath()
	fi, err := os.Stat(dir)
	if err != nil {
		return os.IsNotExist(err) && task.cloneRepository()
	}

	if !fi.IsDir() {
		return false
	}

	git := filepath.Join(dir, ".git")
	if _, err = os.Stat(git); err != nil {
		os.RemoveAll(dir)
		return task.cloneRepository()
	}

	return task.updateRepository()
}

func (task *task) updateRepository() (r bool) {
	for i := 0; i < 3; i++ {
		if err := shell0(gitMax, "", "git", "-C", task.diskPath(), "clean", "-fd"); err != nil {
			errorf("FAIL err=%v", err)
			return false
		}

		var out []byte
		var err error
		switch {
		case task.branch != "":
			if err := shell0(gitMax, "", "git", "-C", task.diskPath(), "fetch"); err != nil {
				errorf("FAIL err=%v", err)
				return false
			}

			if out, err = shell(gitMax, "", "git", "-C", task.diskPath(), "checkout", "-f", task.branch); err != nil {
				errorf("FAIL err=%v", err)
				return false
			}

			if err := shell0(gitMax, "", "git", "-C", task.diskPath(), "clean", "-fd"); err != nil {
				errorf("FAIL err=%v", err)
				return false
			}

			return true
		default:
			if out, err = shell(gitMax, "", "git", "-C", task.diskPath(), "checkout", "-f"); err != nil {
				errorf("FAIL err=%v", err)
				return false
			}
		}

		if s := string(out); strings.Contains(s, "is ahead") ||
			strings.Contains(s, "diverged") {
			if err = shell0(gitMax, "", "git", "-C", task.diskPath(), "reset", "--hard", "HEAD~1"); err != nil {
				errorf("FAIL err=%v", err)
				return false
			}

			continue
		}

		break
	}

	if err := shell0(gitMax, "", "git", "-C", task.diskPath(), "clean", "-fd"); err != nil {
		errorf("FAIL err=%v", err)
		return false
	}

	if err := shell0(gitMax, "", "git", "-C", task.diskPath(), "pull"); err != nil {
		errorf("FAIL err=%v", err)
		return false
	}

	return true
}

func (task *task) cloneRepository() (r bool) {
	if err := os.MkdirAll(task.diskPath(), 0700); err != nil {
		errorf("FAIL err=%v", err)
		return false
	}

	if shell0(gitMax, "", "git", "clone", task.url(), task.diskPath()) != nil {
		return false
	}

	if task.branch == "" {
		return true
	}

	if err := shell0(gitMax, "", "git", "-C", task.diskPath(), "checkout", "-f", task.branch); err != nil {
		errorf("FAIL err=%v", err)
		return false
	}

	return true
}

func (task *task) url() string {
	switch {
	case strings.HasPrefix(task.importPath, "modernc.org/"):
		return fmt.Sprintf("https://gitlab.com/cznic/%s.git", task.importPath[len("modernc.org/"):])
	case task.importPath == "gonum.org/v1/gonum/v1/gonum":
		return "https://github.com/gonum/gonum.git"
	case task.importPath == "gonum.org/v1/gonum/v1/plot":
		return "https://github.com/gonum/plot.git"
	case task.importPath == "zombiezen.com/go/sqlite":
		return "https://github.com/zombiezen/go-sqlite.git"
	case strings.HasPrefix(task.importPath, "git.sr.ht/"):
		return fmt.Sprintf("https://%s", task.importPath)
	default:
		return fmt.Sprintf("https://%s.git", task.importPath)
	}
}

type echoWriter struct {
	w      bytes.Buffer
	silent bool
}

func (w *echoWriter) Write(b []byte) (int, error) {
	if !w.silent {
		os.Stderr.Write(b)
	}
	return w.w.Write(b)
}

func timeNow() string { return time.Now().Format(time.RFC1123) }

func initTasks() {
	defer func() {
		src := rand.NewSource(time.Now().UnixNano())
		g := rand.New(src)
		for i := range tasks {
			j := g.Intn(len(tasks))
			tasks[i], tasks[j] = tasks[j], tasks[i]
		}
	}()

	for _, builder := range []string{
		"test",

		// qemu
		"e5-1650",
		"freebsd64",
		"freebsd_arm64",
		"openbsd64-75",
		"s390x",
		"win32",
		// paused "freebsd-386",
		// paused "freebsd_arm", // MIA
		"netbsd64",
		// paused "netbsd_386",
		// paused "netbsd_arm",
		// paused "omnios64",
		// paused "openbsd64",
		// paused "openbsd_386",
		// paused "openbsd_arm64",
		// replaced  "linux_loong64",

		// HW
		"darwin",
		"darwin-m1",
		"nuc64",
		"pi32",
		"pi400",
		"pi64",
		"win64",

		// remote
		"linux_loong64b",
		"ppc64le",
		"riscv64",
		// paused "mips64le",
	} {
		for _, importPath := range []string{
			"modernc.org/b",
			"modernc.org/bitz",
			"modernc.org/cc/v4",
			"modernc.org/ccgo/v4",
			"modernc.org/css",
			"modernc.org/db",
			"modernc.org/dyd",
			"modernc.org/egg",
			"modernc.org/equ",
			"modernc.org/file",
			"modernc.org/fileutil",
			"modernc.org/fsm",
			"modernc.org/gc/v2",
			"modernc.org/gc/v3",
			"modernc.org/go0",
			"modernc.org/internal",
			"modernc.org/knuth",
			"modernc.org/libX11",
			"modernc.org/libXau",
			"modernc.org/libXdmcp",
			"modernc.org/libXft",
			"modernc.org/libXrender",
			"modernc.org/libbsd",
			"modernc.org/libc",
			"modernc.org/libexpat",
			"modernc.org/libfontconfig",
			"modernc.org/libfreetype",
			"modernc.org/libgmp",
			"modernc.org/libmd",
			"modernc.org/libmpc",
			"modernc.org/libmpfr",
			"modernc.org/libpcre",
			"modernc.org/libpcre16",
			"modernc.org/libpcre2-16",
			"modernc.org/libpcre2-32",
			"modernc.org/libpcre2-8",
			"modernc.org/libpcre2-posix",
			"modernc.org/libpcre32",
			"modernc.org/libpcreposix",
			"modernc.org/libqbe",
			"modernc.org/libquickjs",
			"modernc.org/libsamplerate",
			"modernc.org/libsqlite3",
			"modernc.org/libsqlite_vec",
			"modernc.org/libtcl8.6",
			"modernc.org/libtcl9.0",
			"modernc.org/libtk9.0",
			"modernc.org/libxcb",
			"modernc.org/libz",
			"modernc.org/mathutil",
			"modernc.org/memory",
			"modernc.org/opt",
			"modernc.org/ql",
			"modernc.org/quickjs",
			"modernc.org/rec",
			"modernc.org/regexp",
			"modernc.org/sortutil",
			"modernc.org/sqlite",
			"modernc.org/strutil",
			"modernc.org/tcl8.6",
			"modernc.org/tcl9.0",
			"modernc.org/tfs",
			"modernc.org/tk9.0",
			"modernc.org/visualmd",
			// obsolete "modernc.org/cc/v3",
			// obsolete "modernc.org/cc/v5",
			// obsolete "modernc.org/ccgo/v3",
			// obsolete "modernc.org/ccorpus",
			// obsolete "modernc.org/libadvapi32",
			// obsolete "modernc.org/libcomctl32",
			// obsolete "modernc.org/libcomdlg32",
			// obsolete "modernc.org/libgdi32",
			// obsolete "modernc.org/libimm32",
			// obsolete "modernc.org/libkernel32",
			// obsolete "modernc.org/libnetapi32",
			// obsolete "modernc.org/libole32",
			// obsolete "modernc.org/libshell32",
			// obsolete "modernc.org/libuser32",
			// obsolete "modernc.org/libuserenv",
			// obsolete "modernc.org/libwinspool",
			// obsolete "modernc.org/libws2_32",
			// obsolete "modernc.org/tcl",
			// obsolete "modernc.org/z",
			// paused "git.sr.ht/~jackmordaunt/go-libwebp",
			// paused "gitea.arsenm.dev/Arsen6331/pcre",
			// paused "github.com/edsrzf/mmap-go",
			// paused "github.com/ericlagergren/decimal",
			// paused "github.com/josharian/secretrabbit",
			// paused "github.com/ncruces/go-sqlite3",
			// paused "github.com/pbnjay/memory",
			// paused "github.com/remyoudompheng/bigfft",
			// paused "github.com/shopspring/decimal",
			// paused "gonum.org/v1/gonum/v1/gonum",
			// paused "gonum.org/v1/gonum/v1/plot",
			// paused "modernc.org/ace",
			// paused "modernc.org/assets",
			// paused "modernc.org/ccorpus2",
			// paused "modernc.org/ebnf",
			// paused "modernc.org/ebnfutil",
			// paused "modernc.org/gc/v3/internal/ebnf",
			// paused "modernc.org/golex",
			// paused "modernc.org/goyacc",
			// paused "modernc.org/hash",
			// paused "modernc.org/httpfs",
			// paused "modernc.org/immutable",
			// paused "modernc.org/kv",
			// paused "modernc.org/lex",
			// paused "modernc.org/lexer",
			// paused "modernc.org/lldb",
			// paused "modernc.org/ngrab",
			// paused "modernc.org/parser",
			// paused "modernc.org/qbe",
			// paused "modernc.org/readline",
			// paused "modernc.org/run",
			// paused "modernc.org/scanner",
			// paused "modernc.org/scannertest",
			// paused "modernc.org/token",
			// paused "modernc.org/uncomment",
			// paused "modernc.org/xc",
			// paused "modernc.org/y",
			// paused "modernc.org/yy",
			// paused "modernc.org/zappy",
			// paused "zombiezen.com/go/sqlite",
		} {
			task := task{builder: builder, importPath: importPath, testPath: importPath}
			switch importPath {
			case "git.sr.ht/~jackmordaunt/go-libwebp":
				task.test = []string{"go", "build", "./cmd", "./lib", "./webp"}
			case "github.com/ncruces/go-sqlite3":
				task.test = []string{"go", "test", "-vet", "off", "-failfast", "-timeout", tmax, "./..."}
			case "gonum.org/v1/gonum/v1/gonum":
				task.test = []string{"go", "test", "-vet", "off", "-failfast", "-timeout", tmax, "./..."}
			case "modernc.org/b":
				task.test = []string{"go", "test", "-vet", "off", "-failfast", "-timeout", tmax, "./..."}
			case "modernc.org/dyd":
				task.test = []string{"go", "test", "-vet", "off", "-failfast", "-timeout", tmax, ".", "./dyd"}
			case "modernc.org/run":
				task.test = []string{"go", "test", "-vet", "off", "-failfast", "-timeout", tmax, "./..."}
			case "modernc.org/internal":
				task.test = []string{"go", "test", "-vet", "off", "-failfast", "-timeout", tmax, "./..."}
			case "modernc.org/qbe":
				task.test = []string{"go", "test", "-vet", "off", "-v", "-failfast", "-timeout", tmax, "./...", "-bestof", "1", "-gcc", "7,8,9,10,11"}
			case "zombiezen.com/go/sqlite":
				task.test = []string{"go", "test", "-vet", "off", "-failfast", "-timeout", tmax, "./..."}
			case "modernc.org/scanner":
				task.test = []string{"go", "test", "-vet", "off", "-failfast", "-timeout", tmax, "./..."}
			case "modernc.org/parser":
				task.test = []string{"go", "test", "-vet", "off", "-failfast", "-timeout", tmax, "./..."}
			case "modernc.org/lexer":
				task.test = []string{"go", "test", "-vet", "off", "-failfast", "-timeout", tmax, "./..."}
			case "modernc.org/knuth":
				task.test = []string{"go", "test", "-vet", "off", "-failfast", "-timeout", tmax, "./..."}
			case "modernc.org/egg":
				task.test = []string{"go", "test", "-vet", "off", "-failfast", "-timeout", tmax, "./..."}
			case "modernc.org/regexp":
				task.test = []string{"go", "test", "-vet", "off", "-failfast", "-timeout", tmax, "./..."}
			case "modernc.org/rec":
				task.test = []string{"go", "test", "-vet", "off", "-failfast", "-timeout", tmax, "./..."}
			case "modernc.org/tfs":
				task.test = []string{"go", "test", "-vet", "off", "-failfast", "-timeout", tmax, "./..."}
			case "modernc.org/gc/v2":
				task.importPath = "modernc.org/gc"
				task.testPath = "modernc.org/gc/v2"
			case "modernc.org/gc/v3":
				task.importPath = "modernc.org/gc"
				task.testPath = "modernc.org/gc/v3"
			case "modernc.org/gc/v3/internal/ebnf":
				task.importPath = "modernc.org/gc"
				task.testPath = "modernc.org/gc/v3/internal/ebnf"
			case "modernc.org/cc/v3":
				task.importPath = "modernc.org/cc"
				task.testPath = "modernc.org/cc/v3"
			case "modernc.org/cc/v4":
				task.importPath = "modernc.org/cc"
				task.testPath = "modernc.org/cc/v4"
			case "modernc.org/cc/v5":
				task.importPath = "modernc.org/cc"
				task.testPath = "modernc.org/cc/v5"
			case "modernc.org/ccgo/v3":
				task.importPath = "modernc.org/ccgo"
				task.testPath = "modernc.org/ccgo/v3/lib"
				task.test = []string{"go", "test", "-vet", "off", "-failfast", "-timeout", tmax, "-tags=ccgo.assert"}
			case "modernc.org/ngrab":
				task.importPath = "modernc.org/ngrab"
				task.testPath = "modernc.org/ngrab/lib"
			case "modernc.org/ccgo/v4":
				task.importPath = "modernc.org/ccgo"
				task.testPath = "modernc.org/ccgo/v4/lib"
				task.test = []string{"go", "test", "-vet", "off", "-failfast", "-timeout", tmax, "-tags=ccgo.assert"}
			}
			tasks = append(tasks, task)
		}
	}

	//TODO- // These are tested only to build.
	//TODO- for _, builder := range []string{
	//TODO- 	"darwin-m1",
	//TODO- 	"e5-1650",
	//TODO- 	"nuc32",
	//TODO- 	"nuc64",
	//TODO- 	"pi32",
	//TODO- 	"pi64",
	//TODO- 	"test",
	//TODO- } {
	//TODO- 	for _, importPath := range []string{
	//TODO- 		"modernc.org/expat",
	//TODO- 		"modernc.org/fontconfig",
	//TODO- 		"modernc.org/freetype",
	//TODO- 		"modernc.org/gettext",
	//TODO- 		"modernc.org/tk",
	//TODO- 		"modernc.org/x11",
	//TODO- 		"modernc.org/xau",
	//TODO- 		"modernc.org/xcb",
	//TODO- 		"modernc.org/xdmcp",
	//TODO- 		"modernc.org/xft",
	//TODO- 		"modernc.org/xrender",
	//TODO- 	} {
	//TODO- 		task := task{builder: builder, importPath: importPath, testPath: importPath}
	//TODO- 		task.test = []string{"go", "build", "./..."}
	//TODO- 		tasks = append(tasks, task)
	//TODO- 	}
	//TODO- }
	//TODO- // These are tested only to build.
	//TODO- for _, builder := range []string{
	//TODO- 	"e5-1650",
	//TODO- 	"nuc32",
	//TODO- 	"nuc64",
	//TODO- 	"pi32",
	//TODO- 	"pi64",
	//TODO- 	"test",
	//TODO- } {
	//TODO- 	for _, importPath := range []string{
	//TODO- 		"modernc.org/atk",
	//TODO- 	} {
	//TODO- 		task := task{builder: builder, importPath: importPath, testPath: importPath}
	//TODO- 		task.test = []string{"go", "build", "./..."}
	//TODO- 		tasks = append(tasks, task)
	//TODO- 	}
	//TODO- }
}

func cleanRepo() {
	const max = 3
	for i := 0; i < max; i++ {
		infof("cleanRepo #%d", i)
		out, err := shell(gitMax, "", "git", "status")
		if err != nil {
			infof("cleanRepo: git status: %v", err)
			continue
		}

		if s := string(out); !strings.Contains(s, "modified:") &&
			!strings.Contains(s, "have diverged") &&
			!strings.Contains(s, "is ahead") &&
			!strings.Contains(s, "Changes not staged for commit:") {
			infof("cleanRepo: ok\n%s", s)
			return
		}

		if _, err = shell(gitMax, "", "git", "reset", "--hard", "HEAD~1"); err == nil {
			infof("cleanRepo: git reset ok")
			return
		}

		infof("cleanRepo: git reset: %v", err)
	}
}

func shell0(limit time.Duration, inDir, argv0 string, args ...string) (err error) {
	_, err = shell(limit, inDir, argv0, args...)
	return err
}

func shell(limit time.Duration, inDir, argv0 string, args ...string) (out []byte, err error) {
	if filepath.Base(argv0) == "git" {
	out:
		for _, v := range args {
			switch v {
			case "pull" /* , "push" */ :
				gitNetLimiter()
				break out
			}
		}
	}
	if inDir != "" && inDir != "." {
		cwd, err := os.Getwd()
		if err != nil {
			gerror(err)
			return nil, err
		}

		if err = os.Chdir(inDir); err != nil {
			gerror(err)
			return nil, err
		}

		defer func() {
			if err2 := os.Chdir(cwd); err2 != nil {
				gerror(err2)
				if err == nil {
					err = err2
				}
			}
		}()
	}

	if argv0, err = exec.LookPath(argv0); err != nil {
		return nil, err
	}

	var b echoWriter
	b.silent = !*oStream
	ctx, cancel := context.WithTimeout(context.Background(), limit)

	defer cancel()

	c := exec.CommandContext(ctx, argv0, args...)
	c.WaitDelay = limit + 1*time.Minute
	c.Stdout = &b
	c.Stderr = &b
	infof("run shell(limit=%v, inDir=%v, argv0=%v, args=%v)", limit, inDir, argv0, args)
	err = c.Run()
	out = b.w.Bytes()
	infof("shell(limit=%v, inDir=%v, argv0=%v, args=%v)\n%s\nerr=%v", limit, inDir, argv0, args, out, err)
	return out, err
}

func (t *task) autoTasks(cfg *builderCfg, dir string) (out []byte, err error) {
	defer func() {
		if *oLogAuto || err != nil {
			infof("%s: AUTO tasks: out=%s err=%s (%v: %v: %v:)", t.testPath, out, err, origin(4), origin(3), origin(2))
		}
	}()

	if t.branch != "" {
		infof("AUTO tasks disabled: branch=%s", t.branch)
		return nil, nil
	}

	autotag, err := targetMatches(target, cfg.Autotag)
	if err != nil {
		return nil, err
	}

	autogen, err := targetMatches(target, cfg.Autogen)
	if err != nil {
		return nil, err
	}

	autoupdate, err := targetMatches(target, cfg.Autoupdate)
	if err != nil {
		return nil, err
	}

	if *oDBG {
		infof("AUTOTASKS autotag=%v autogen=%v autoupdate=%v", autotag, autogen, autoupdate)
	}
	wd, err := os.Getwd()
	if err != nil {
		return nil, err
	}

	if err := os.Chdir(dir); err != nil {
		return nil, err
	}

	defer func() {
		if e := os.Chdir(wd); e != nil {
			err = e
		}
	}()

	s0 := t.importPath
	pkg := filepath.Base(s0)
	if strings.HasPrefix(pkg, "v") && len(pkg) > 1 && pkg[1] >= '0' && pkg[1] <= '9' {
		dir, _ = filepath.Split(s0)
		pkg = filepath.Base(dir)
	}

	var haveMod, wantMod []byte
	var haveFn string
	root := filepath.Join("internal", "autogen")
	if autogen {
		// Is this an auto tasks package?
		fi, err := os.Stat(root)
		if err != nil {
			if os.IsNotExist(err) {
				goto more
			}
			return nil, err
		}

		if !fi.IsDir() {
			return nil, fmt.Errorf("%s exists, but is not a directory", root)
		}

		if fi, err = os.Stat(".gitignore"); err != nil || !fi.Mode().IsRegular() {
			return nil, fmt.Errorf("missing or irregular .gitignore")
		}

		if wantMod, err = os.ReadFile("go.mod"); err != nil {
			return nil, err
		}

		haveFn = filepath.Join(root, fmt.Sprintf("%s_%s.mod", goos, goarch))
		if haveMod, err = os.ReadFile(haveFn); err != nil {
			if !os.IsNotExist(err) {
				return nil, err
			}
		}
	}

more:
	if autotag {
		// 1. auto tag
		if out, err = t.autotag(cfg, dir); err != nil {
			return out, err
		}
	}

	if autoupdate {
		if out, err = t.autodeps(dir); err != nil {
			return out, err
		}

		if t.modUpdated {
			if wantMod, err = os.ReadFile("go.mod"); err != nil {
				return nil, err
			}
		}
	}

	if !autogen {
		return nil, nil
	}

	// 3. auto generate. Are we outdated?
	if bytes.Equal(wantMod, haveMod) {
		return nil, nil
	}

	if builder == "test" {
		infof("SKIP auto generate: builder=%v", builder)
		return nil, nil
	}

	infof("haveMod\n%s", haveMod)
	infof("wantMod\n%s", wantMod)

	for _, v := range cfg.Download {
		download, err := targetMatches(target, v.Re)
		if err != nil {
			return nil, fmt.Errorf("regexp.Compile(%q) -> FAIL err=%v", v.Re, err)
		}

		if !download {
			continue
		}

		for _, url := range v.Files {
			// Is the archive already downloaded?
			url := strings.TrimSpace(url)
			if url == "" {
				continue
			}

			urlBase := filepath.Base(url)
			switch fi, err := os.Stat(urlBase); {
			case err != nil:
				if !os.IsNotExist(err) {
					return nil, fmt.Errorf("stat %s: %s", urlBase, err)
				}

				switch target {
				case "windows/arm64":
					if out, err = shell(time.Hour, "", "wget", "--no-check-certificate", url); err != nil {
						return out, err
					}
				default:
					if out, err = shell(time.Hour, "", "wget", url); err != nil {
						return out, err
					}
				}
			default:
				if fi.Mode()&os.ModeType != 0 {
					return nil, fmt.Errorf("%s exists but it's not a regular file: %#0o", urlBase, fi.Mode())
				}
			}
		}
	}

	tempDir := filepath.Join(os.TempDir(), pkg)
	os.Setenv("GO_GENERATE_DIR", tempDir)
	os.MkdirAll(tempDir, 0770)
	switch goos {
	case "windows":
		if out, err = shell(4*time.Hour, "", "git-bash", "-c", "go generate"); err != nil {
			return out, err
		}

		if _, err := os.Stat("unconvert.sh"); err == nil {
			shell(time.Hour, "", "git-bash", "-c", "./unconvert.sh")
		}
	default:
		if out, err = shell(4*time.Hour, "", "go", "generate"); err != nil {
			return out, err
		}

		if _, err := os.Stat("unconvert.sh"); err == nil {
			shell(time.Hour, "", "sh", "-c", "./unconvert.sh")
		}
	}

	shell(gitMax, "", "git", "branch", "-D", "autogen")
	if out, err = shell(gitMax, "", "git", "checkout", "-b", "autogen"); err != nil {
		return out, err
	}

	// go generate succeeded, record the go.mod used.
	if err := os.WriteFile(haveFn, wantMod, 0660); err != nil {
		return nil, err
	}

	if out, err = shell(gitMax, "", "git", "add", "."); err != nil {
		return out, err
	}

	msg := fmt.Sprintf("%s: auto generate", builder)
	if out, err = shell(gitMax, "", "git", "commit", "-am", msg); err != nil {
		return out, err
	}

	if out, err = shell(gitMax, "", "git", "checkout", "master"); err != nil {
		return out, err
	}

	if out, err = shell(gitMax, "", "git", "pull"); err != nil {
		return out, err
	}

	if out, err = shell(gitMax, "", "git", "merge", "--squash", "autogen"); err != nil {
		return out, err
	}

	if out, err = shell(gitMax, "", "git", "commit", "-am", msg); err != nil {
		return out, err
	}

	if out, err = shell(gitMax, "", "git", "push"); err != nil {
		return nil, err
	}

	commit, err := t.commitHash()
	if err != nil {
		return nil, err
	}

	infof("%s: AUTO generate: OK", t.testPath)
	t.hash = commit
	t.needTest = true
	return nil, nil
}

// func run(timeout time.Duration, argv ...string) (out []byte, err error) {
// 	ctx, cancel := context.WithTimeout(context.Background(), timeout)
//
// 	defer cancel()
//
// 	cmd := exec.CommandContext(ctx, argv[0], argv[1:]...)
// 	cmd.WaitDelay = time.Minute
// 	return cmd.CombinedOutput()
// }

func (t *task) autotag(cfg *builderCfg, dir string) (out []byte, err error) {
	var newTag string

	defer func() {
		if *oLogAuto || err != nil {
			infof("%s: AUTO tag: newTag=%s out=%s err=%s (%v: %v: %v:)", t.testPath, newTag, out, err, origin(4), origin(3), origin(2))
		}
	}()

	const mtag = "modernc.org"
	asBuilder := builder
	if asBuilder == "test" {
		asBuilder = leader
	}
	if asBuilder != leader {
		return nil, nil
	}

	wd, err := os.Getwd()
	if err != nil {
		return nil, err
	}

	if *oDBG {
		infof("AUTOTAG task=%+v dir=%v wd=%v", t, dir, wd)
	}
	if out, err = shell(gitMax, "", "git", "tag"); err != nil {
		return out, err
	}

	tags := strings.Split(string(out), "\n")
	if *oDBG {
		infof("tags=%v", tags)
	}
	if len(tags) == 0 {
		return nil, fmt.Errorf("auto tag: %s has no tags", t.testPath)
	}

	semver.Sort(tags)
	hasTag := semver.Canonical(tags[len(tags)-1])
	if !semver.IsValid(hasTag) {
		if hasTag != "" {
			return nil, fmt.Errorf("auto tag: %s hasTag=%s is invalid", t.testPath, hasTag)
		}
	}

	tagParts := strings.Split(hasTag, ".")
	if len(tagParts) == 3 {
		if *oDBG {
			infof("hasTag=%v tagParts=%q", hasTag, tagParts)
		}
		n, err := strconv.ParseInt(tagParts[2], 10, 32)
		if err != nil {
			// return nil, fmt.Errorf("%s: invalid version %s", t.testPath, hasTag)

			// The above disrupts auto task on seeing tags like "vx.y.z-rc1". Instead give up only the auto tag sub-task.
			return nil, nil
		}

		tagParts[2] = fmt.Sprint(n + 1)
		newTag = strings.Join(tagParts, ".")
	}
	if *oDBG {
		infof("newTag=%v", newTag)
	}

	out, err = shell(goMax, "", "gorelease")
	if *oDBG {
		infof("gorelease: out=%s err=%v", out, err)
	}
	if err == nil {
		lines := strings.Split(strings.TrimSpace(string(out)), "\n")
		if len(lines) != 0 {
			line := lines[len(lines)-1]
			const tag = "Suggested version: "
			if strings.HasPrefix(line, tag) {
				s := strings.TrimSpace(line[len(tag):])
				if semver.IsValid(s) {
					newTag = s
					if *oDBG {
						infof("newTag=%s", newTag)
					}
				}
			}
		}
	}

	switch {
	case hasTag == "":
		newTag = "v0.0.1"
	default:
		tagHash, err := t.tagHash(hasTag)
		if err != nil {
			return nil, err
		}

		commitHash, err := t.commitHash()
		if err != nil {
			return nil, err
		}

		if tagHash == commitHash {
			infof("%s already tagged %s", commitHash, hasTag)
			return nil, nil
		}
	}

	for strings.Contains(dir, mtag) && filepath.Base(dir) != mtag {
		dir, _ = filepath.Split(filepath.Clean(dir))
	}

	dir, _ = filepath.Split(filepath.Clean(dir))
	if *oDBG {
		infof("dir=%v", dir)
	}
	if err := os.Chdir(dir); err != nil {
		return nil, err
	}

	var stdout, stderr bytes.Buffer
	u := gomod.NewUpdater(&stdout, &stderr, dir, false, false, true, true)
	if err := u.Run(); err != nil {
		if *oDBG {
			infof("stdout=%s", stdout.Bytes())
			infof("stderr=%s", stderr.Bytes())
			errorf("FAIL err=%s", err)
		}
		return append(stdout.Bytes(), stderr.Bytes()...), err
	}

	infof("stdout=%s", stdout.Bytes())
	infof("stderr=%s", stderr.Bytes())

	ip := t.testPath
	if !strings.HasPrefix(ip, mtag+"/") {
		return nil, fmt.Errorf("internal error: ip=%s", ip)
	}
	for k, v := range u.Updates {
		for _, w := range v {
			if k == ip {
				t.canUpdate = append(t.canUpdate, w)
				infof("%+v", w)
			}
		}
	}

	if *oDBG {
		infof("ip=%s", ip)
	}
	var votes, needVotes int
	for headPath := range u.HeadsNotTagged {
		if *oDBG {
			infof("headPath=%q", headPath)
		}
		headPath = filepath.ToSlash(headPath)
		if !strings.HasPrefix(headPath, mtag+"/") {
			if *oDBG {
				infof("SKIP headPath=%q mtag=%q", headPath, mtag)
			}
			continue
		}

		if !strings.HasPrefix(ip, headPath) {
			if *oDBG {
				infof("SKIP ip=%q headPath=%q", ip, headPath)
			}
			continue
		}

		var re *regexp.Regexp
		if *oDBG {
			infof("headPath=%s not tagged", headPath)
		}
		for k, v := range results {
			if *oDBG {
				infof("k=%v v=%v", k, v)
			}
			if k.testPath != ip {
				if *oDBG {
					infof("continue")
				}
				continue
			}

			switch cfg.Autotag {
			case "":
				// nop here
			case "$libs":
				if !isLib(resultOS(v), resultArch(v)) {
					continue
				}
			case "$libqbe":
				if !isLibqbe(resultOS(v), resultArch(v)) {
					continue
				}
			case "$sqlite":
				if !isSqlite(resultOS(v), resultArch(v)) {
					continue
				}
			default:
				if re == nil {
					if re, err = regexp.Compile(cfg.Autotag); err != nil {
						return nil, fmt.Errorf("regexp.Compile(%q) -> FAIL err=%v", cfg.Autotag, err)
					}
				}

				if !re.MatchString(fmt.Sprintf("%s/%s", resultOS(v), resultArch(v))) {
					continue
				}
			}
			needVotes++
			if *oDBG {
				infof("needVotes++=%v votes=%v k=%s v=%s", needVotes, votes, k, v)
			}

			if resultCommit(v) != t.hash || result(v) != "PASS" {
				continue
			}

			votes++
			if *oDBG {
				infof("needVotes=%v votes++=%v k=%s v=%s", needVotes, votes, k, v)
			}
		}
		break
	}
	if *oDBG {
		infof("needVotes=%v votes=%v", needVotes, votes)
	}
	if err = os.Chdir(wd); err != nil {
		return nil, err
	}

	if votes != 0 && votes == needVotes {
		// if out, err = shell(gitMax, "", "git", "tag"); err != nil {
		// 	return out, err
		// }

		if *oDBG {
			infof("debug: 'git tag' returns\n%s", out)
		}

		if out, err = shell(gitMax, "", "git", "tag", newTag, "-m", "autotag"); err != nil {
			if *oDBG {
				infof("%s FAIL err=%v", out, err)
			}
			return out, err
		}

		if out, err = shell(gitMax, "", "git", "push", "--tags"); err != nil {
			if *oDBG {
				infof("%s FAIL err=%v", out, err)
			}
			return out, err
		}

		infof("%s: AUTO tag: newTag=%s OK", t.testPath, newTag)
	}
	return nil, nil
}

func (t *task) autodeps(dir string) (out []byte, err error) {
	defer func() {
		if *oLogAuto || err != nil {
			infof("%s: AUTO deps: out=%s err=%s (%v: %v: %v:)", t.testPath, out, err, origin(4), origin(3), origin(2))
		}
	}()

	asBuilder := builder
	if asBuilder == "test" {
		asBuilder = leader
	}
	if asBuilder != leader || len(t.canUpdate) == 0 {
		return nil, nil
	}

	wd, err := os.Getwd()
	if err != nil {
		return nil, err
	}

	if err := os.Chdir(dir); err != nil {
		return nil, err
	}

	defer func() {
		if e := os.Chdir(wd); e != nil {
			err = e
		}
	}()

	for _, v := range t.canUpdate {
		if out, err := shell(goMax, "", "go", "get", fmt.Sprintf("%s@%s", v.Module, v.Tag)); err != nil {
			errorf("%s: go get %+v: %s FAIL %s", t.testPath, v, out, err)
			return out, err
		}
	}

	if builder == "test" {
		infof("%s: SKIP uploading", t.testPath)
		return nil, nil
	}

	if out, err = shell(2*goMax, "", "go", "build", "./..."); err != nil {
		errorf("%s: go build: %s FAIL %s", t.testPath, out, err)
		return out, err
	}

	if out, err = shell(gitMax, "", "git", "commit", "-am", fmt.Sprintf("%s: auto deps", builder)); err != nil {
		errorf("%s: git commit: %s FAIL %s", t.testPath, out, err)
		return out, err
	}

	if out, err = shell(gitMax, "", "git", "push"); err != nil {
		errorf("%s: git push: %s FAIL %s", t.testPath, out, err)
		return out, err
	}

	infof("%s: AUTO deps: OK", t.testPath)
	t.modUpdated = true
	t.needTest = true
	return nil, nil
}

func targetMatches(target string, re string) (ok bool, err error) {
	switch re {
	case "":
		return true, nil
	case "$libs":
		for k := range libs {
			if target == k {
				return true, nil
			}
		}
		return false, nil
	case "$libqbe":
		for k := range libqbe {
			if target == k {
				return true, nil
			}
		}
		return false, nil
	case "$sqlite":
		for k := range sqlite {
			if target == k {
				return true, nil
			}
		}
		return false, nil
	default:
		cre, err := regexp.Compile(re)
		if err != nil {
			return false, err
		}

		return cre.MatchString(target), nil
	}
}

type download struct {
	Files []string // URLs
	Re    string   // target match or ""
}

type builderCfg struct {
	Autogen    string // regexp or $var
	Autotag    string // regexp or $var
	Autoupdate string // regexp or $var
	Download   []*download
	Test       string // regexp or $var
}

func loadBuilderConfig(path string) (cfg *builderCfg, err error) {
	defer func() {
		infof("loadBuilderConfig() -> cfg=%s err=%v", strutil.PrettyString(cfg, "", "", nil), err)
	}()

	b, err := os.ReadFile(path)
	if err != nil {
		if os.IsNotExist(err) {
			err = nil
		}
		return nil, err
	}

	cfg = &builderCfg{}
	if err = json.Unmarshal(b, cfg); err != nil {
		cfg = nil
	}
	return cfg, err
}

func (cfg *builderCfg) purgeResults(testPath string) (updated bool) {
	if cfg == nil || cfg.Test == "" {
		infof("return false")
		return false
	}

	if !regexp.MustCompile(cfg.Test).MatchString(target) {
		key := resultKey{builder: builder, testPath: testPath}
		purgeKeys = append(purgeKeys, key)
		infof("scheduled deleting %+v", key)
		needUpload = true
		return true
	}

	infof("return false 2")
	return false
}
